package com.api.file;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "TipoArchivo", schema="db_files.FILES")

public class TipoArchivo {
	
	 @Id
	 @GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	 @GeneratedValue(generator = "generator")
	 @Column(name = "PK_IdTipoArchivo" , columnDefinition="uniqueidentifier")
	 private String PK_IdTipoArchivo;
	 @JoinColumn(name = "FK_IdGrupoTipoArchivo__FILES" , columnDefinition="uniqueidentifier")
	 @ManyToOne
	 private GrupoTipoArchivo FK_IdGrupoTipoArchivo__FILES;
	 @JoinColumn(name = "FK_IdTipoAlmacenamiento__Files" , columnDefinition="uniqueidentifier")
	 @ManyToOne
	 private TipoAlmacenamiento FK_IdTipoAlmacenamiento__Files;
	 private String Nombre;
	 private String Descripcion;
	 private String MimeType;
	 private String Extension;
	 private boolean CT_LIVE;
	 private Date CT_CREATE;
	 private Date CT_UPDATE;
	 
	    
	    
	    public TipoArchivo(){
	    	
	    }
	    
	    public TipoArchivo(String pK_IdTipoArchivo, GrupoTipoArchivo fK_IdGrupoTipoArchivo_FILES
	    		, TipoAlmacenamiento fK_IdTipoAlmacenamiento_Files,String nombre,String descripcion, String mimeType
	    		,String extension, boolean cT_LIVE,Date cT_CREATE, Date cT_UPDATE){
	    	this.PK_IdTipoArchivo=pK_IdTipoArchivo;
	    	this.FK_IdGrupoTipoArchivo__FILES=fK_IdGrupoTipoArchivo_FILES;
	    	this.FK_IdTipoAlmacenamiento__Files=fK_IdTipoAlmacenamiento_Files;
	    	this.Nombre=nombre;
	    	this.Descripcion=descripcion;
	    	this.MimeType=mimeType;
	    	this.Extension=extension;
	    	this.CT_LIVE=cT_LIVE;
	    	this.CT_CREATE=cT_CREATE;
	    	this.CT_UPDATE=cT_UPDATE;
	    	
	    }



		public String getPK_IdTipoArchivo() {
			return PK_IdTipoArchivo;
		}

		public void setPK_IdTipoArchivo(String pK_IdTipoArchivo) {
			PK_IdTipoArchivo = pK_IdTipoArchivo;
		}

		public GrupoTipoArchivo getFK_IdGrupoTipoArchivo_FILES() {
			return FK_IdGrupoTipoArchivo__FILES;
		}

		public void setFK_IdGrupoTipoArchivo_FILES(GrupoTipoArchivo fK_IdGrupoTipoArchivo_FILES) {
			FK_IdGrupoTipoArchivo__FILES = fK_IdGrupoTipoArchivo_FILES;
		}

		public TipoAlmacenamiento getFK_IdTipoAlmacenamiento_Files() {
			return FK_IdTipoAlmacenamiento__Files;
		}

		public void setFK_IdTipoAlmacenamiento_Files(TipoAlmacenamiento fK_IdTipoAlmacenamiento_Files) {
			FK_IdTipoAlmacenamiento__Files = fK_IdTipoAlmacenamiento_Files;
		}

		public String getNombre() {
			return Nombre;
		}

		public void setNombre(String nombre) {
			Nombre = nombre;
		}

		public String getDescripcion() {
			return Descripcion;
		}

		public void setDescripcion(String descripcion) {
			Descripcion = descripcion;
		}

		public String getMimeType() {
			return MimeType;
		}

		public void setMimeType(String mimeType) {
			MimeType = mimeType;
		}

		public String getExtension() {
			return Extension;
		}

		public void setExtension(String extension) {
			Extension = extension;
		}

		public boolean isCT_LIVE() {
			return CT_LIVE;
		}

		public void setCT_LIVE(boolean cT_LIVE) {
			CT_LIVE = cT_LIVE;
		}

		public Date getCT_CREATE() {
			return CT_CREATE;
		}

		public void setCT_CREATE(Date cT_CREATE) {
			CT_CREATE = cT_CREATE;
		}

		public Date getCT_UPDATE() {
			return CT_UPDATE;
		}

		public void setCT_UPDATE(Date cT_UPDATE) {
			CT_UPDATE = cT_UPDATE;
		}

		@Override
	    public String toString() {
	        return "TipoArchivo{" +
	                ", PK_IdTipoArchivo='" + PK_IdTipoArchivo + '\'' +
	                 ", FK_IdGrupoTipoArchivo_FILES='" + FK_IdGrupoTipoArchivo__FILES + '\'' +
	                  ", FK_IdTipoAlmacenamiento_Files='" + FK_IdTipoAlmacenamiento__Files + '\'' +
	                   ", Nombre='" + Nombre + '\'' +
	                    ", Descripcion='" + Descripcion + '\'' +
	                     ", MimeType='" + MimeType + '\'' +
	                      ", Extension='" + Extension + '\'' +
	                       ", CT_LIVE='" + CT_LIVE + '\'' +
	                       ", CT_CREATE='" + CT_CREATE + '\'' +
	                ", CT_UPDATE=" + CT_UPDATE +
	                '}';
	    }
		
		
		
	    
}
