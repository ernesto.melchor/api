package com.api.file.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.api.file.TipoArchivo;

public interface TipoArchivoRepository extends JpaRepository<TipoArchivo, Integer> {
	
}
